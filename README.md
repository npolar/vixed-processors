# VIXED processors

This repo contains a multi-processor library written in python 3 which can be
used in combination with [VIXED](https://gitlab.npolar.no/eds/gis-and-eo/vixed)
to generate satellite imagery- or forecast products that are size optimized and
deliverable through our mail service.

The processor products that are offered are:
- `sar`: SAR imagery for sea ice products obtained from [Copernicus Scientific Data
  Hub](https://colhub.met.no/).
- `chl-a`: Sentinel-3 imagery for Chl-a products obtained from [Copernicus Online Data
  ACCESS](https://coda.eumetsat.int/).
- `yr-forecast`: Yr-forecast obtained from [Meteorologisk Institutt](https://api.met.no/weatherapi/locationforecast/3.0).


## Structure

The processor is initiated by `run-processor.py` along with a log-file, an
output-file with the correct extension and a request file of type json at root
level, which utilizes the pyvixed library to process the request and generate
the product.

```
.
├── Dockerfile
├── examples
│   ├── sar-request.json
│   ├── sentinel-request.json
│   └── yr-request.json
├── Makefile
├── pyvixed
│   ├── chla.py
│   ├── __init__.py
│   ├── processors.py
│   ├── __pycache__
│   ├── request.py
│   ├── sar.py
│   ├── utils.py
│   └── yr.py
├── README.md
├── requirements-dev.txt
├── requirements.txt
├── run-processor.py
└── tests
    ├── __pycache__
    ├── test_e2e.py
    ├── test_processors.py
    ├── test_request.py
    └── test_utils.py
```
## Developement

### Dependencies

To run the processor locally, GDAL and python packages found in
`requirements.txt` has to be installed.

Installation of GDAL using using apt:

```
apt install gdal-bin
```

Installation of python packages using pip3:

```
pip3 install -r requirements.txt
```

In order to run the `Makefile` commands for python that enables the usage of
pytest and linting/formatting tools, the requirements found in
`requirements-dev.txt` needs to be installed. Installation using pip3:

```
pip3 install -r requirements-dev.txt
```


### Usage

To run the processor locally from the command line, each individual processor
require the following CLI-arguments:

- `sar`:
  - Credentials for [Copernicus Scientific Data Hub](https://colhub.met.no/) as
  `USER_SAR=username PASSWORD_SAR=password`
  - request file as json
  - output-file with tiff/tif file extention
  - log file

```
USER_SAR=username PASSWORD_SAR=password python3 ./run-processor.py --log-file=log.txt --request-file=request.json --output-file=out.tif
```

- `chl-a`
  - Credentials for [Copernicus Online Data ACCESS](https://coda.eumetsat.int/)
  as `USER_CHLA=username PASSWORD_CHLA=password`
  - request file as json
  - output-file with tiff/tif file extention
  - log file

```
USER_CHLA=username PASSWORD_CHLA=password python3 ./run-processor.py --log-file=log.txt --request-file=request.json --output-file=out.tif
```

- `yr-forecast`
  - request file as json
  - output-file with file extention of textfile format
  - log file

```
python3 ./run-processor.py --log-file=log.txt --request-file=request.json --output-file=out.txt
```

Examples of json-files with valid requests for each processor can be found in
the `examples`-folder.

### Testing

The `Makefile` contains instructions to perform unittests and generate coverage
reports to ensure good code quality.

The test suite is split into:

- `unittest`, which is a testsuite for the methods and functions of the
  `pyvixed` library.
- `make test_e2e`, which performs an end-to-end test for each processor,
  ensuring that it behaves as expected with respect to what the user
  requirements.

Perform testing by one of the following commands:

```
make test
make unittest
make test_e2e
make cov
make htmlcov
```

In which the first test is a combination of the two tests, and `cov` and
`htmlcov` generates a coverage report in the terminal and as html, respectively.

To view the html-report, type in root level:

```
coverage html
<browser> htmlcov/index.html
```
