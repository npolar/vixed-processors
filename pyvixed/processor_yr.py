import urllib.request

import xmltodict

from pyvixed.processor import Processor


def make_forecast_message(mlist, step=3, days=3):
    """Format forecast message"""
    full_message = "".join(
        [
            "---\nWeather forecast by MET Norway, delivered by NPI\n",
            "WARNING: valid only at sea level\n",
            "Lat: {}, Lon: {}, Elev: {}\n".format(
                mlist[0]['location']['@latitude'],
                mlist[0]['location']['@longitude'],
                mlist[0]['location']['@altitude']),
            "---\n\n"])
    for elem in mlist[::step][0:int(24 / step * days)]:
        message = "".join([
            'Date: {}\n'.format(elem['@to']),
            '\tTemperature: {} deg C, Pressure: {} HPa, Wind: {} m/s {}\n\n'.format(
                elem['location']['temperature']['@value'],
                elem['location']['pressure']['@value'],
                elem['location']['windSpeed']['@mps'],
                elem['location']['windDirection']['@name'])])
        full_message += message

    return full_message


class ProcessorYrForecast(Processor):
    def __init__(
            self,
            request_data,
            credentials,
            logger,
            output_file,
            keep_temp):
        """Initialize processor"""

        logger.debug("Starting yr-forecast processor")
        constant_parameters = {"User-Agent": "data.npolar.no"}
        request_data = dict(request_data, **constant_parameters)

        Processor.__init__(
            self,
            request_data,
            credentials,
            logger,
            keep_temp,
            output_file,
            output_dir=None,
        )

    def _get_data(self):
        """Obtain data from api using parameters from the request file"""

        api_url = "https://api.met.no/weatherapi/locationforecast/3.0/classic?lat={}&lon={}".format(
            float(self._request_data['lat']), float(self._request_data['lon']))
        try:
            api_request = urllib.request.Request(
                api_url,
                headers={
                    "User-Agent": self._request_data['User-Agent'],
                }
            )
            response = urllib.request.urlopen(api_request)
            weather_dict = xmltodict.parse(response.read())
            # acquire parts of the response which contain wind (it also has
            # temperature, pressure, etc)
            self.wind = [d for d in weather_dict['weatherdata']
                         ['product']['time'] if 'windSpeed' in d['location']]
            self._logger.info('Retrieved forecast successfully')
        except BaseException:
            self._logger.info('Could not retrieve forecast')
            raise

    def _process_data(self):
        """Write formatted forecast message to output text-file"""

        with open(self._output_file, 'w') as ofile:
            ofile.write(make_forecast_message(self.wind))
