import datetime
import json
import logging
import multiprocessing
import os
from functools import partial

import affine
import geojson
import numpy as np
import rasterio
from dateutil import parser as date_parser
from pyproj import Proj
from sentinelsat import SentinelAPI, geojson_to_wkt
from shapely import wkt
from shapely.geometry import Polygon

from pyvixed import processor_factory

#### TRANFORMATION AND CONVERSION UTILITIES ##############################


def convert_coords(coords, crs):
    """
    Convert coordinates from geographic to projected coordinates

    Args:
    coords -- list of polygon vertices
    crs -- string with epsg projection code

    Returns:
    xy_list -- lists of projected coordinates in x- and y-direction
    """
    crs = str(crs).upper()
    trg_proj = Proj(crs)
    xy_list = [trg_proj(c[0], c[1]) for c in coords[0]]
    return xy_list


def affine_transformation(rcoords, res, crs):
    """
    Create affine-style geotransformation and calculate height and width

    Args:
    rcoords -- list of requested polygon vertices
    res -- integer with pixel width (resolution)
    crs -- string with epsg projection code

    Returns:
    aff -- affine transform
    height -- float with image height
    width -- float with image width
    """
    coords = convert_coords(rcoords, crs)
    x_array = np.array([c[0] for c in coords])
    y_array = np.array([c[1] for c in coords])

    aff = affine.Affine(
        res,  # pixel width
        0,  # rotation
        x_array.min(),  # x - upper left
        0,  # column rotation
        -1 * res,  # pixed height
        y_array.max(),  # y - upper left
    )

    height = (y_array.max() - y_array.min()) / res
    width = (x_array.max() - x_array.min()) / res
    return aff, height, width


#### IMAGE AQUISITION AND PROCESSING UTILITIES ###########################


def create_empty_dst(output_file, rcoords, res, crs, dtype, nodata):
    """
    Create empty geotiff-dataset as boundaries for the
    satellite data

    Args:
    output-file -- string with output filename
    rcoords -- list of requested polygon vetices
    res -- interger with pixil width (resolution)
    crs -- string with epsg projection code
    dtype -- data type object
    nodata -- no-data value
    """

    aff, height, width = affine_transformation(rcoords, res, crs)

    rasterio.open(
        output_file,
        "w",
        driver="GTiff",
        height=height,
        width=width,
        count=1,
        dtype=dtype,
        crs=crs,
        transform=aff,
        photometric="RGB",
        nodata=nodata,
        compress="LZW",
        predict=2,
    )


def process_request(request, logger, output_file, creds, keep_temp):
    """
    Perform processing of user requests to vixed.  The data processor is mapped
    to the one indicated in the request file.

    Args:
    request -- dictionary containing the request parameters
    logger -- log file initialization
    output_file -- destination for processor product
    creds -- user credentials required for obtaining source data
    keep_temp -- flag to switch on keep temporary files-functionality
    """
    processor = processor_factory.get_processor(
        request, creds, logger, output_file, keep_temp
    )
    processor.exec()


def download_sentinel_product(pkey, api, output_dir, logger):
    """
    Download single sentinel product.
    """

    logger.info("Obtaining item {}".format(pkey))
    api.download(pkey, directory_path=output_dir)


def sentinel_query(logger, credentials, request_data, output_dir, **kwargs):
    """
    Construct query based on request for downloading sentinel products.
    """
    footprint = geojson_to_wkt(request_data["roi"])
    logger.debug("Search footprint: {}".format(footprint))

    try:
        end_time = date_parser.parse(request_data["end_time"])
    except KeyError:
        logger.info(
            "Request does not specify end_time, using current timestamp")
        end_time = datetime.datetime.utcnow()

    start_time = end_time - \
        datetime.timedelta(hours=request_data["time_delta_hours"])

    api_url = request_data["api_url"]

    # Connect to the API
    api = SentinelAPI(
        credentials["user"],
        credentials["password"],
        api_url=api_url,
        show_progressbars=True,
        timeout=100,
    )

    api.session.timeout = 100
    api.session.verify = False
    products = api.query(footprint, date=(start_time, end_time), **kwargs)
    n_scenes = len(products.keys())
    logger.info("Found {} scenes".format(n_scenes))

    if n_scenes > 0:
        logger.info(
            "Scenes list:\n{}".format(
                "\n".join(
                    ["\t" + products[key]["identifier"] for key in products.keys()]
                )
            )
        )
    else:
        logger.info("No scenes found")
        raise SystemExit("Aborting.")

    # Download product(s)
    pool = multiprocessing.Pool(processes=1)
    pool.map(
        partial(
            download_sentinel_product,
            api=api,
            output_dir=output_dir,
            logger=logger),
        products.keys(),
    )

    return products


def create_logger(logger_name, log_file, level=None):
    """
    Create a new logger. When no level is specified, the log level is set to
    DEBUG.
    """
    logger = logging.getLogger(logger_name)
    if level is not None:
        logger.setLevel(level)
    else:
        logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter(
        "%(asctime)s  %(name)s  %(levelname)s: %(message)s")

    file_handler = logging.FileHandler(log_file)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)

    return logger


#### VERIFICATION UTILITIES ##############################################


def verify_fsize_and_nodata(logger, output_file):
    """
    Verify that product is smaller than 2 MB and that product contains data. If
    not, a message is returned to the user with instructions for modification of
    the request.
    """
    # Verify that filesize is smaller than 2 MB
    fsize = os.stat(output_file).st_size
    try:
        assert fsize < 2e6
    except AssertionError:
        logger.error(
            "The requested file size is too big. Try to decrease ROI or resolution"
        )

    # Verify that image contains data
    _, res_nodata, res_array = extract_dst_data(output_file)
    try:
        assert np.all(res_array == res_nodata) == False
    except AssertionError:
        logger.error(
            "Obtained image contains no data. Try to increase ROI or time window"
        )


def get_env_var(env_var):
    """
    Get environmental variable (mainly username and password). If it's not
    provided, an exception is raised.
    """
    try:
        return os.environ[env_var]
    except KeyError:
        logger.info("Variable missing: {}".format(env_var))
        raise Exception("Can't proceed without variable: {}".format(env_var))


def check_exists(log_file=None, output_file=None, request_file=None):
    """
    Verify that the required command line arguments are provided. The procesor
    can't run without them.
    """
    # Initialize logger
    global logger

    if not log_file:
        raise Exception("Logger is not initialized, exiting.")
    else:
        logger = create_logger("vixed", log_file)

    # Check that output file is provided
    if not output_file:
        logger.info("Output file missing, aborting")
        raise Exception("Can't proceed without output file")
    else:
        output = output_file

    # Check that request file is provided and return content
    if not request_file:
        logger.info("Request file missing, aborting")
        raise Exception("Can't proceed without request json file")
    else:
        with open(request_file) as rf:
            request = json.loads(rf.read())
    return logger, output, request


#### TESTING UTILITIES ###################################################


def time_in_range(start, end, x):
    """Return true if x is in the range [start, end]"""
    if start <= end:
        return start <= x <= end
    else:
        return start <= x or x <= end


def extract_dst_data(output_file):
    """
    Extract result coordinates as polygon and nodata-value and array for
    testing
    """
    with rasterio.open(output_file) as dst:
        res_bounds = dst.bounds
        corners = (
            (res_bounds.left, res_bounds.top),
            (res_bounds.right, res_bounds.top),
            (res_bounds.right, res_bounds.bottom),
            (res_bounds.left, res_bounds.bottom),
        )
        res_array = dst.read(1)
        dst.crs
        res_nodata = dst.nodata

    res_poly = Polygon(corners)

    return res_poly, res_nodata, res_array


def create_polygon(request_data):
    """Create polygon from requested coordinates"""
    with open(request_data) as f:
        req_bounds = json.load(f)
    req_crs = req_bounds["crs"]
    roi = geojson_to_wkt(req_bounds["roi"])
    roi_feature = geojson.Feature(geometry=wkt.loads(roi), properties={})
    req_coords = roi_feature["geometry"]["coordinates"]
    coords = convert_coords(req_coords, req_crs)

    polygon = Polygon(coords)

    return polygon
