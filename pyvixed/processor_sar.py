import glob
import os
import re
import subprocess
import tempfile
from zipfile import ZipFile

import numpy
import rasterio

from pyvixed import utils
from pyvixed.processor import Processor


def percentile_peak(ifile):
    """Find max-value in source file"""
    with rasterio.open(ifile, "r") as dst:
        ubound = numpy.percentile(dst.read(1), 99)
    return ubound


class ProcessorSAR(Processor):
    def __init__(
        self,
        request_data,
        credentials,
        logger,
        output_file,
        keep_temp,
    ):
        """Initialize processor"""

        constant_parameters = {
            "api_url": "https://colhub.met.no/",
            "platformname": "Sentinel-1",
            "producttype": "GRD",
            "sensoroperationalmode": "EW",
            "compress": "True",
        }

        request_data = dict(request_data, **constant_parameters)

        output_dir = output_file + ".d"
        logger.debug("Output path: {}".format(output_dir))
        if not os.path.exists(output_dir):
            os.mkdir(output_dir)

        Processor.__init__(
            self,
            request_data,
            credentials,
            logger,
            keep_temp,
            output_file,
            output_dir,
        )

    def _get_data(self):
        """Obtain data from api using parameters from the request file"""

        ice_query = dict(
            platformname=self._request_data["platformname"],
            producttype=self._request_data["producttype"],
            sensoroperationalmode=self._request_data["sensoroperationalmode"],
        )
        self.products = utils.sentinel_query(
            self._logger,
            self._credentials,
            self._request_data,
            self._output_dir,
            **ice_query
        )

    def _process_single_scene(self, product, tfile):
        """Extract the contents of the obtained file and save to output_dir"""

        with ZipFile(
            os.path.join(self._output_dir, ".".join([product["identifier"], "zip"]))
        ) as zf:
            zf.extractall(self._output_dir)
            input_paths = glob.glob(os.path.join(self._output_dir, ".".join(
                [product["identifier"], "SAFE/measurement/*.tiff"])))
            for i, ifile in enumerate(input_paths):
                if re.search(r".*(vh).*.tiff|.*(hh).*.tiff", ifile):
                    with tempfile.NamedTemporaryFile() as temp:
                        uboundval = percentile_peak(ifile)
                        subprocess.call(
                            [
                                "gdal_translate",
                                "-scale",
                                "0",
                                "{}".format(int(uboundval)),
                                "0",
                                "255",
                                "-ot",
                                "Byte",
                                ifile,
                                temp.name,
                            ]
                        )

                        subprocess.call(
                            [
                                "gdalwarp",
                                "-t_srs",
                                self._request_data["crs"],
                                "-srcnodata",
                                "0",
                                "-dstnodata",
                                "0",
                                "-r",
                                "bilinear",
                                temp.name,
                                tfile,
                            ]
                        )

    def _process_data(self):
        """Resample the obtained data to a GeoTiff image"""

        with tempfile.NamedTemporaryFile() as tfile:
            utils.create_empty_dst(
                tfile.name,
                self._request_data["roi"]["coordinates"],
                self._request_data["spatial_resolution"],
                self._request_data["crs"],
                rasterio.uint8,
                0,
            )

            for product_key in self.products.keys():
                self._process_single_scene(
                    self.products[product_key],
                    tfile.name
                )

            subprocess.call(
                [
                    "gdal_translate",
                    "-ot",
                    "Byte",
                    "-co",
                    "COMPRESS=JPEG",
                    "-co",
                    "JPEG_QUALITY=70",
                    "-scale",
                    tfile.name,
                    self._output_file,
                ]
            )

        # Verify that filesize is smaller than 2 MB and that the image contains
        # data
        utils.verify_fsize_and_nodata(self._logger, self._output_file)
