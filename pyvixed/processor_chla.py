import glob
import os
import tempfile
from zipfile import ZipFile

import numpy as np
import rasterio
import xarray as xr
from pyresample import kd_tree
from pyresample.geometry import AreaDefinition, SwathDefinition

from pyvixed import utils
from pyvixed.processor import Processor


def extract_time(dst):
    """Extract timestamp by splitting the dataset filename

    Filenames are received on the format:
    S3B_OL_2_WFR____20211110T081521_20211110T081821_20211110T100633_0179_059_092_3960_MAR_O_NR_003
    """
    return dst.split("_")[7]


class ProcessorChlA(Processor):
    def __init__(
            self,
            request_data,
            credentials,
            logger,
            output_file,
            keep_temp):
        """Initialize processor"""

        constant_parameters = {
            "api_url": "https://coda.eumetsat.int/",
            "platformname": "Sentinel-3",
            "timeliness": "Near Real Time",
            "producttype": "OL_2_WFR___",
            "instrumentshortname": "OLCI",
        }

        request_data = dict(request_data, **constant_parameters)

        output_dir = output_file + ".d"
        logger.debug("Output path: {}".format(output_dir))
        if not os.path.exists(output_dir):
            os.mkdir(output_dir)

        Processor.__init__(
            self,
            request_data,
            credentials,
            logger,
            keep_temp,
            output_file,
            output_dir,
        )

    def _get_data(self):
        """Obtain data from api using parameters from the request file"""
        chla_query = dict(
            platformname=self._request_data["platformname"],
            producttype=self._request_data["producttype"],
            timeliness=self._request_data["timeliness"],
            instrumentshortname=self._request_data["instrumentshortname"],
        )
        _ = utils.sentinel_query(
            self._logger,
            self._credentials,
            self._request_data,
            self._output_dir,
            **chla_query
        )

    def _process_data(self):
        """Resample the obtained data to a GeoTiff image"""
        nc_fname = "chl_oc4me.nc"
        with tempfile.TemporaryDirectory() as unzip:
            for file in glob.glob(self._output_dir + "/*.zip"):
                with ZipFile(file) as zip_ref:
                    self._logger.debug(
                        "Unzipping {} to {}".format(
                            file, unzip))
                    zip_ref.extractall(str(unzip))

                for file in os.listdir(unzip):
                    fname = str(file.strip(".SEN3/"))
                    _chla = xr.open_dataset(
                        os.path.join(unzip, file, nc_fname))
                    _chla = 10 ** (_chla)
                    _chla = _chla.fillna(-999)
                    _coords = xr.open_dataset(os.path.join(
                        unzip, file, "geo_coordinates.nc"))
                    dst = xr.Dataset(
                        {
                            "chl_oc4me": (["x", "y"], _chla.CHL_OC4ME.values),
                        },
                        coords={
                            "lon": (["x", "y"], _coords.longitude.values),
                            "lat": (["x", "y"], _coords.latitude.values),
                        },
                    )
                    dst.attrs = _chla.CHL_OC4ME.attrs
                    dst.to_netcdf(
                        os.path.join(
                            self._output_dir,
                            fname + "_" + nc_fname))

        rcoords = self._request_data["roi"]["coordinates"]
        res = self._request_data["spatial_resolution"]
        crs = self._request_data["crs"]

        # Create area definition:
        area_id = "req"
        coords = utils.convert_coords(rcoords, crs)
        x_array = np.array([c[0] for c in coords])
        y_array = np.array([c[1] for c in coords])
        height = (y_array.max() - y_array.min()) / res
        width = (x_array.max() - x_array.min()) / res
        shape = [int(height), int(width)]
        upper_left_extent = [x_array.min(), y_array.max()]

        area_def = AreaDefinition.from_ul_corner(
            area_id=area_id,
            projection=crs,
            upper_left_extent=upper_left_extent,
            resolution=res,
            shape=shape,
        )

        # Resample netcdf files and create geotiff
        result_exists = False
        sorted_list = glob.glob(os.path.join(self._output_dir, "*.nc"))
        sorted_list.sort(key=extract_time)
        for file in sorted_list:
            dst = xr.open_dataset(file)
            self._logger.debug("Resampling dataset: {}".format(file))
            chl_a = dst["chl_oc4me"].values
            lon = dst["lon"].values
            lat = dst["lat"].values

            swath_def = SwathDefinition(lons=lon, lats=lat)
            current_result = kd_tree.resample_nearest(
                swath_def,
                chl_a,
                area_def,
                fill_value=-999,
                radius_of_influence=5000,
                nprocs=4,
            )
            if not result_exists:
                result = current_result
                result_exists = True
            else:
                result = np.where(
                    current_result != -999, current_result, result)

        # Create empty geotiff
        utils.create_empty_dst(self._output_file, rcoords, res,
                               crs, rasterio.float32, -999)
        # Write data to geotiff
        with rasterio.open(self._output_file, "r+") as o_dst:
            o_dst.update_tags(
                START_TIME=str(extract_time(sorted_list[0])),
                END_TIME=str(extract_time(sorted_list[-1])),
                UNITS="mg.m-3",
            )
            o_dst.write(result, 1)

        # Verify that filesize is smaller than 2 MB and that the image contains
        # data
        utils.verify_fsize_and_nodata(self._logger, self._output_file)
