import os
import shutil


class Processor(object):
    def __init__(
        self,
        request_data,
        credentials,
        logger,
        keep_temp,
        output_file,
        output_dir=None,
    ):
        """Initialize processor

        Initialization follows the specifications received through request
        file and command line
        """
        self._request_data = request_data
        self._credentials = credentials
        self._keep_temp = keep_temp
        self._output_file = output_file
        self._output_dir = output_dir
        self._logger = logger
        self.name = request_data["processor"]

    def _get_data(self):
        """Obtain data relevant to the processor

        Refer to the desired processor to see processor-specific implementation.
        """

    def _process_data(self):
        """Process data relevant to the processor

        Refer to the desired processor to see processor-specific implementation.
        """

    def _cleanup(self):
        """Clean up temporary files

        If keep_temp-flag is not provided when initializing the processor, the
        temporary files will be removed after the processor execution.
        """
        if self._keep_temp:
            if self._output_dir:
                self._logger.debug(
                    "Keep temporary directory {}".format(
                        self._output_dir))
            else:
                self._logger.debut("Temporary directory does not exist")
        else:
            if self._output_dir:
                self._logger.debug(
                    "Removing temporary directory {}".format(
                        self._output_dir))
                for root, dirs, files in os.walk(self._output_dir):
                    for f in files:
                        os.unlink(os.path.join(root, f))
                    for d in dirs:
                        shutil.rmtree(os.path.join(root, d))
                    os.rmdir(root)

    def exec(self):
        """Execute processor"""
        self._get_data()
        self._process_data()
        self._cleanup()
