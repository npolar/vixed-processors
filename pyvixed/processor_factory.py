from pyvixed.processor_chla import ProcessorChlA
from pyvixed.processor_sar import ProcessorSAR
from pyvixed.processor_yr import ProcessorYrForecast


def get_processor(request, creds, logger, output_file, keep_temp):
    """Initialize requested processor"""
    processors = {
        "sar": ProcessorSAR,
        "chl-a": ProcessorChlA,
        "yr-forecast": ProcessorYrForecast,
    }
    return processors[request["processor"]](
        request, creds, logger, output_file, keep_temp
    )
