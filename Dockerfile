FROM ubuntu:20.04
MAINTAINER "data.npolar.no"

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update \
  && apt install -y python3-pip python3-dev \
  && pip3 install --upgrade pip

ADD ./* /app/
WORKDIR /app

RUN pip3 install -r requirements.txt

ENTRYPOINT ["echo", "hello"]
