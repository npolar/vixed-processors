import logging
import tempfile
import unittest

from pyvixed.processor_chla import ProcessorChlA
from pyvixed.processor_factory import get_processor
from pyvixed.processor_sar import ProcessorSAR
from pyvixed.processor_yr import ProcessorYrForecast


class TestProcessorFactory(unittest.TestCase):
    def test_factory_returns_processor(self):
        request = {
            "processor": "chl-a"
        }
        creds = {}
#        logger = create_logger("pyvixed", level='CRITICAL')
        logger = logging.getLogger("pyvixed")
        keep_temp = False
        with tempfile.NamedTemporaryFile() as outfile:
            processor = get_processor(request, creds, logger, outfile.name,
                                      keep_temp)
            self.assertIsInstance(processor, ProcessorChlA)

    def test_init_all_processors(self):
        #        logger = create_logger("pyvixed", level='CRITICAL')
        keep_temp = False
        logger = logging.getLogger("pyvixed")
        processors = {
            "sar": ProcessorSAR,
            "chl-a": ProcessorChlA,
            "yr-forecast": ProcessorYrForecast
        }
        with tempfile.NamedTemporaryFile() as outfile:
            for k in processors.keys():
                self.assertEqual(get_processor(
                    {"processor": k}, {}, logger, outfile.name, keep_temp).name, k
                )


if __name__ == "__main__":
    unittest.main()
