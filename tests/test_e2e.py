import datetime
import imghdr
import json
import os
import re
import tempfile
import unittest
from argparse import Namespace

import numpy as np
from dateutil import parser as date_parser

from pyvixed.utils import (check_exists, create_logger, create_polygon,
                           extract_dst_data, process_request, time_in_range)


class TestCaseBase(unittest.TestCase):
    """Custom test class for new test methods"""

    def assertFileNotExists(self, path):
        """
        Assert the existence of the processor product.
        """
        if not os.path.exists(path):
            raise AssertionError("File does not exist: %s" % str(path))

    def assertIsGeoTiff(self, path):
        """
        Assert that the sar/sentinel product is a tiff.
        """
        assert imghdr.what(path) == "tiff", "Image is not of type tiff"

    def assertROIMatches(self, res_poly, req_poly):
        """
        Assert whether settings and resulting polygons overlap in one of the
        following ways:
        - polygons have more than one but not all points in common (overlap).
        - polygons intersect.
        """
        try:
            assert req_poly.overlaps(
                res_poly
            ), "Settings polygon is not overlapping result polygon"
        except AssertionError:
            assert req_poly.intersects(
                res_poly
            ), "Settings polygon does not intersect with result polygon"

    def assertGeotTiffNotEmpty(self, array, nodata):
        """
        Assert that the sar/sentinel product is not empty.
        """
        assert np.all(array == nodata) == False, "Image contains no data"

    def assertTimeStampMatches(self, request_data, logger):
        """
        Assert that the acquisition is within the specified time range
        """
        with open(request_data) as f:
            timestamp = json.load(f)
            try:
                end_time = date_parser.parse(timestamp["end_time"])
            except KeyError:
                end_time = datetime.datetime.utcnow()

            start_time = end_time - datetime.timedelta(
                hours=timestamp["time_delta_hours"]
            )

        acquisition_time = []
        with open(logger, "r") as log:
            log_lines = log.readlines()
            for l in log_lines:
                timestamps = re.findall(r"[0-9]{8}T[0-9]{6}", l)
                if len(timestamps) > 0:
                    acquisition_time.append(timestamps[0])
        for time in acquisition_time:
            t = datetime.datetime.strptime(time, "%Y%m%dT%H%M%S")
            assert time_in_range(
                start_time, end_time, t
            ), "Acquistion is not within specified time range"

    def assertPositionMatches(self, request_data, path):
        """
        Assert that the settings and resulting positions match.
        """
        with open(path, "r") as f:
            outfile = f.read()
            res_lat = re.search("(?<=Lat: )(\\w+)", outfile).group()
            res_lon = re.search("(?<=Lon: )(\\w+)", outfile).group()
        with open(request_data) as f:
            req_bounds = json.load(f)
            req_lat = req_bounds["lat"]
            req_lon = req_bounds["lon"]
        assert (
            int(res_lat) == req_lat and int(res_lon) == req_lon
        ), "Requested and result position does not match"

    def assertLogNoScenesFound(self, logger):
        """
        Assert that if no scenes were found, it will be logged and the
        information will reach the user.
        """
        with open(logger, "r") as log:
            log_lines = log.readlines()
            self.assertEqual("No scenes found\n",
                             log_lines[-1].rsplit("INFO: ")[-1])


class TestRequestProcessing(TestCaseBase):
    """
    This set of tests contains end-to-end tests
    for executing real life processors in natural habitat
    """

    def test_processing_yr_request(self):
        creds = {"user": "", "password": ""}
        request_data = {"processor": "yr-forecast", "lat": 77, "lon": 13}
        keep_temp = False
        with tempfile.TemporaryDirectory() as tdir:
            cli_args = Namespace()
            cli_args.request_file = os.path.join(tdir, "request")
            cli_args.output_file = os.path.join(tdir, "output")
            cli_args.log_file = os.path.join(tdir, "log")

            logger = create_logger(
                "pyvixed-yr", log_file=cli_args.log_file, level="CRITICAL"
            )

            with open(cli_args.request_file, "w") as fp:
                fp.write(json.dumps(request_data))

            log, output_file, request = check_exists(
                cli_args.log_file, cli_args.output_file, cli_args.request_file
            )

            process_request(
                request,
                logger,
                output_file,
                creds,
                keep_temp,
            )
            self.assertFileNotExists(output_file)
            self.assertPositionMatches(
                cli_args.request_file, cli_args.output_file)

    def test_processing_sar_request(self):
        creds = {"user": os.environ["USER_SAR"],
                 "password": os.environ["PASSWORD_SAR"]}
        request_data = {
            "processor": "sar",
            "time_delta_hours": 3,
            "spatial_resolution": 300,
            "roi": {
                "coordinates": [
                    [[23, 82], [23, 82.2], [23.2, 82.2], [23.2, 82], [23, 82]]
                ],
                "type": "Polygon",
            },
            "crs": "EPSG:32633",
        }
        keep_temp = False
        with tempfile.TemporaryDirectory() as tdir:
            cli_args = Namespace()
            cli_args.request_file = os.path.join(tdir, "request")
            cli_args.output_file = os.path.join(tdir, "output")
            cli_args.log_file = os.path.join(tdir, "log")

            logger = create_logger(
                "pyvixed-sar", log_file=cli_args.log_file, level="INFO"
            )
            with open(cli_args.request_file, "w") as fp:
                fp.write(json.dumps(request_data))

            log, output_file, request = check_exists(
                cli_args.log_file, cli_args.output_file, cli_args.request_file
            )

            try:
                process_request(
                    request,
                    logger,
                    output_file,
                    creds,
                    keep_temp,
                )

                self.assertTimeStampMatches(
                    cli_args.request_file, cli_args.log_file)
                res_poly, res_nodata, res_array = extract_dst_data(
                    cli_args.output_file)
                req_poly = create_polygon(
                    cli_args.request_file,
                )
                self.assertFileNotExists(cli_args.output_file)
                self.assertIsGeoTiff(cli_args.output_file)
                self.assertROIMatches(res_poly, req_poly)
                self.assertGeotTiffNotEmpty(res_array, res_nodata)

            except SystemExit:
                self.assertLogNoScenesFound(cli_args.log_file)

    def test_processing_chl_a_request(self):
        """
        test_processing_chl_a_request should retrieve chla-data, if any exists,
        based on the specified polygon.
        """
        creds = {
            "user": os.environ["USER_CHLA"],
            "password": os.environ["PASSWORD_CHLA"],
        }
        request_data = {
            "processor": "chl-a",
            "time_delta_hours": 3,
            "spatial_resolution": 300,
            "roi": {
                "coordinates": [
                    [[12, -22], [12.7, -22], [12.7, -23], [12, -23], [12, -22]]
                ],
                "type": "Polygon",
            },
            "crs": "EPSG:32633",
        }
        keep_temp = False
        with tempfile.TemporaryDirectory() as tdir:
            cli_args = Namespace()
            cli_args.request_file = os.path.join(tdir, "request")
            cli_args.output_file = os.path.join(tdir, "output")
            cli_args.log_file = os.path.join(tdir, "log")

            logger = create_logger(
                "pyvixed-chla", log_file=cli_args.log_file, level="INFO"
            )
            with open(cli_args.request_file, "w") as fp:
                fp.write(json.dumps(request_data))

            log, output_file, request = check_exists(
                cli_args.log_file, cli_args.output_file, cli_args.request_file
            )

            try:
                process_request(
                    request,
                    logger,
                    output_file,
                    creds,
                    keep_temp,
                )

                res_poly, res_nodata, res_array = extract_dst_data(
                    cli_args.output_file)
                req_poly = create_polygon(
                    cli_args.request_file,
                )
                self.assertFileNotExists(cli_args.output_file)
                self.assertIsGeoTiff(cli_args.output_file)
                self.assertROIMatches(res_poly, req_poly)
                self.assertGeotTiffNotEmpty(res_array, res_nodata)

            except SystemExit:
                self.assertLogNoScenesFound(cli_args.log_file)

    def test_processing_chl_a_request_fails(self):
        """
        This test should fail based on the polygon defined, which is in
        continental Antarctica. This is to assert that a failing retrieval of
        data does not cause test-failure.
        """
        creds = {
            "user": os.environ["USER_CHLA"],
            "password": os.environ["PASSWORD_CHLA"],
        }
        request_data = {
            "processor": "chl-a",
            "time_delta_hours": 3,
            "spatial_resolution": 300,
            "roi": {
                "coordinates": [
                    [[71, -81], [71.1, -81], [71.1, -81.1], [71, -81.1], [71, -81]]
                ],
                "type": "Polygon",
            },
            "crs": "EPSG:32633",
        }
        keep_temp = False
        with tempfile.TemporaryDirectory() as tdir:
            cli_args = Namespace()
            cli_args.request_file = os.path.join(tdir, "request")
            cli_args.output_file = os.path.join(tdir, "output")
            cli_args.log_file = os.path.join(tdir, "log")

            logger = create_logger(
                "pyvixed-chla_failing",
                log_file=cli_args.log_file,
                level="INFO")
            with open(cli_args.request_file, "w") as fp:
                fp.write(json.dumps(request_data))

            log, output_file, request = check_exists(
                cli_args.log_file, cli_args.output_file, cli_args.request_file
            )

            try:
                process_request(
                    request,
                    logger,
                    output_file,
                    creds,
                    keep_temp,
                )

            except SystemExit:
                self.assertLogNoScenesFound(cli_args.log_file)
