import os
import tempfile
import unittest

import numpy as np

from pyvixed import utils


class TestCoordinatesConversion(unittest.TestCase):
    def test_conversion(self):
        crs = "EPSG:3857"
        coords = [
            [
                [10, 78],
                [10, 79],
                [11, 79],
                [11, 78],
                [10, 78]
            ]
        ]
        expected_result = [
            (1113194.9079327357, 14368684.28280032),
            (1113194.9079327357, 14927396.251835858),
            (1224514.3987260093, 14927396.251835858),
            (1224514.3987260093, 14368684.28280032),
            (1113194.9079327357, 14368684.28280032),
        ]

        result = utils.convert_coords(coords, crs)
        self.assertAlmostEqual(result, expected_result)

    def test_empty_dst_created(self):
        """
        Test that an empty geotiff is created.
        """
        coords = [
            [
                [10, 78],
                [10, 79],
                [11, 79],
                [11, 78],
                [10, 78]
            ]
        ]
        crs = "EPSG:3857"
        res = 300
        dtype = np.uint8
        nodata = 99

        with tempfile.TemporaryDirectory() as tdir:
            tfile = os.path.join(tdir, "tfile.tif")
            utils.create_empty_dst(
                tfile, coords, res, crs, dtype, nodata)
            assert os.path.isfile(tfile)
            _, res_nodata, res_array = utils.extract_dst_data(tfile)
            assert np.all(res_array == res_nodata)

    def test_create_logger(self):
        with tempfile.TemporaryDirectory() as tdir:
            logfile = os.path.join(tdir, "test")
            with self.assertLogs("test", level="DEBUG") as cm:
                logger = utils.create_logger("test", logfile, level="DEBUG")
                logger.error('Test logger')
            self.assertEqual(cm.output, ['ERROR:test:Test logger'])


if __name__ == "__main__":
    unittest.main()
