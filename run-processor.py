#!/usr/bin/env python3
import argparse

from pyvixed.utils import check_exists, get_env_var, process_request


def main():
    """
    Initialization of processors prividing data to the Vixed mailing service.
    """
    p = argparse.ArgumentParser()
    p.add_argument("--log-file", default=None)
    p.add_argument("--output-file", default=None)
    p.add_argument("--request-file", default=None)
    p.add_argument(
        "-k",
        "--keep-temporary",
        help="Store temporary files",
        action="store_true")

    args = p.parse_args()

    logger, output_file, request = check_exists(
        args.log_file, args.output_file, args.request_file)

    # Get correct credentials based on  processor name
    processor_name = request["processor"]
    if processor_name == "chl-a":
        credentials = dict(
            user=get_env_var('USER_CHLA'),
            password=get_env_var('PASSWORD_CHLA')
        )
    elif processor_name == "sar":
        credentials = dict(
            user=get_env_var('USER_SAR'),
            password=get_env_var('PASSWORD_SAR')
        )
    elif processor_name == "yr-forecast":
        credentials = dict(
            user="",
            password=""
        )
    else:
        logger.info("No processor name found, aborting")
        raise Exception("Can't proceed without processor name")
    # Process request and exit gracefully
    process_request(
        request,
        logger,
        output_file,
        credentials,
        keep_temp=args.keep_temporary,
    )


if __name__ == "__main__":
    main()
