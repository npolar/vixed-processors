.PHONY: install unittest
src = $(wildcard *.py) \
	  $(wildcard pyvixed/*.py) \
	  $(wildcard tests/*.py)

default: unittest

test: unittest test_e2e

decrypt-secrets:
		echo ${VAULT_PASS} > ~/.vault-pass && ansible-vault view .secrets --vault-password-file=~/.vault-pass > .secrets.decrypted

unittest:
		PYTHONPATH=. pytest tests/test_utils.py tests/test_processors.py

test_e2e:
		PYTHONPATH=. pytest tests/test_e2e.py

autolint: autopep8 autoflake

autopep8:
	    PYTHONPATH=. autopep8 -a -a --in-place $(src)

autoflake:
		PYTHONPATH=. autoflake --in-place --remove-all-unused-imports --remove-unused-variables $(src)
		isort $(src)
lint:
	    PYTHONPATH=. flake8 --select=F $(src)
cov:
	    PYTHONPATH=. pytest --cov=pyvixed tests/

htmlcov:
		PYTHONPATH=. pytest --cov=pyvixed --cov-report=html tests/
